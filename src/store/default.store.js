import { defineStore } from 'pinia'

export const defaultStore = defineStore({
  id: 'default',
  state: () => ({
    count: 0,
  }),
  getters: {
    doubleCount() {
      return this.count * 2
    }
  }
})