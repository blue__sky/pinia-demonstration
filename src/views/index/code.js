export const installCode = `
yarn add pinia
# or with npm
npm install pinia
`

export const useCode = `
import { createPinia } from 'pinia'
app.use(createPinia())
`