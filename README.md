## 轻量级状态管理库 Pinia 教学与演示项目

技术栈： Vue3 + Vite + Pinia2 + NaiveUI

目的：此项目是 Pinia 的实践与演示项目，基本涵盖了pinia的大部分使用场景，可以让伙伴更快的上手此框架。

下载代码后，安装依赖
```shell
yarn install
```

启动项目
```shell
yarn dev
```

内容展示：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/174730_d30891f3_4964818.png "屏幕截图.png")

项目展示gif：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/172001_695060c9_4964818.gif "20211120_171540.gif")

使用说明：clone 代码之后，使用 yarn 安装依赖，随后使用 npm run dev 即可启动项目

Pinia简介：轻量级状态管理库

- 完整的TypeScript 支持
- 完整的类型标支持
- 极其轻巧，体积约1KB
- Store中的Actions 配置项可以执行同步或异步方法
- 模块不需要嵌套，可以声明多个Store 
- 支持Vue DevTools， SSR和Webpack 代码拆分

Pinia 作者是 Vuex 核心开发人员之一，由于Pinea是轻量级的，体积很小，它适合于中小型应用。它也适用于低复杂度的Vue.js项目。将 Vuex 用于中小型 Vue.js 项目是过度的，因为它重量级的，对性能降低有很大影响。因此，Vuex 适用于大规模、高复杂度的 Vue.js 项目。

这个项目是个人的作品，难免会有问题和 BUG，如果有问题请进行评论，我也会尽力去更新，自己也在前端学习的路上，欢迎交流，非常感谢！